package models;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Eko Kurniawan Khannedy
 * @since 24/11/15
 */
public class Login {

    @NotBlank(message = "username tidak boleh kosong")
    public String username;

    @NotBlank(message = "password tidak boleh kosong")
    public String password;

}
