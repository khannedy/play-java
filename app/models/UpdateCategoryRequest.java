package models;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Eko Kurniawan Khannedy
 * @since 25/11/15
 */
public class UpdateCategoryRequest {

    @Length(max = 100, message = "Panjang maksimal 100 karakter")
    @NotBlank(message = "Tidak boleh kosong")
    public String nama;

}
