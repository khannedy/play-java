package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Eko Kurniawan Khannedy
 * @since 25/11/15
 */
@Entity
@Table(name = "tabel_api_key")
public class ApiKey {

    @Id
    @Column(name = "id", length = 100)
    public String id;

    @Column(name = "user_id", length = 100, nullable = false)
    public String user_id;

}
