package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.List;

/**
 * @author Eko Kurniawan Khannedy
 * @since 23/11/15
 */
@Entity
@Table(name = "tabel_kategori")
public class Category {

    @Id
    @Column(length = 100, name = "id")
    public String id;

    @Length(max = 100, message = "Panjang maksimal 100 karakter")
    @NotBlank(message = "Tidak boleh kosong")
    @Column(nullable = false, length = 100, name = "nama")
    public String nama;

    @JsonIgnore
    @OneToMany(mappedBy = "kategori")
    public List<Product> products;

}
