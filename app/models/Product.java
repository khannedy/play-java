package models;

import javax.persistence.*;

/**
 * @author Eko Kurniawan Khannedy
 * @since 25/11/15
 */
@Entity
@Table(name = "tabel_produk")
public class Product {

    @Id
    @Column(name = "id")
    public String id;

    @Column(name = "nama")
    public String nama;

    @Column(name = "harga")
    public Long harga;

    @Column(name = "stok")
    public Integer stok;

    @ManyToOne
    @JoinColumn(name = "kategori_id")
    public Category kategori;

}
