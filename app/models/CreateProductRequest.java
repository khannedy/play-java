package models;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;

/**
 * @author Eko Kurniawan Khannedy
 * @since 25/11/15
 */
public class CreateProductRequest {

    @NotBlank
    @Length(max = 100)
    public String nama;

    @NotBlank
    public String idKategori;

    @Min(1)
    public Long harga;

    @Min(0)
    public Integer stok;

    public Product toProduct() {
        Product product = new Product();
        product.harga = harga;
        product.nama = nama;
        product.stok = stok;
        return product;
    }

}
