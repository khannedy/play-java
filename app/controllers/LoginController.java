package controllers;

import models.Category;
import models.Login;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.hello;
import views.html.index;

import java.util.ArrayList;
import java.util.List;

public class LoginController extends Controller {

    public Result login(){
        Form<Login> form = Form.form(Login.class).fill(new Login());
        return ok(views.html.login.login.render(form));
    }

    public Result tryLogin(){
        Form<Login> form = Form.form(Login.class).bindFromRequest(request().body().asFormUrlEncoded());
        if(form.hasErrors()){
            // error
            return ok(views.html.login.login.render(form));
        }else{
            Login login = form.get();
            // silahkan lakukan pengecekan

            session("login", "ok");

            return redirect(routes.CategoryController.index());
        }
    }

    public Result logout(){
        session().clear();
        return redirect(routes.LoginController.login());
    }

}
