package controllers;

import models.Category;
import play.*;
import play.db.jpa.Transactional;
import play.mvc.*;

import views.html.*;

import java.util.ArrayList;
import java.util.List;

public class Application extends Controller {

    @Transactional
    public Result categories() {
        List<Category> list = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Category c = new Category();
            c.id = String.valueOf(i);
            c.nama = "Kategori-" + i;

            list.add(c);
        }

        return ok(
                views.html.categories.index.render(list)
        );
    }

    public Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public Result hello(String nama) {
        return ok(
                hello.render(nama, 1000)
        );
    }

}
