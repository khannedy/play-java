package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import filters.LoginApiFilter;
import models.ApiKey;
import models.Login;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.util.UUID;

public class LoginRestfull extends Controller {

    @Transactional
    public Result login() {
        JsonNode json = request().body().asJson();
        Form<Login> form = Form.form(Login.class).bind(json);

        if (form.hasErrors()) {
            // error
            return unauthorized(form.errorsAsJson());

        } else {
            Login login = form.get();
            // silahkan lakukan pengecekan

            ApiKey apiKey = new ApiKey();
            apiKey.id = UUID.randomUUID().toString();
            apiKey.user_id = login.username;

            JPA.em().persist(apiKey);

            return ok(Json.toJson(apiKey));
        }
    }

    @Transactional
    @With(LoginApiFilter.class)
    public Result logout() {
        String id = request().getHeader("Api-Key");
        ApiKey apiKey = JPA.em().find(ApiKey.class, id);
        JPA.em().remove(apiKey);

        return ok();
    }

}
