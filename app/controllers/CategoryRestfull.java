package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import filters.LoginApiFilter;
import models.Category;
import models.UpdateCategoryRequest;
import org.springframework.beans.BeanUtils;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import views.html.hello;
import views.html.index;

import java.util.ArrayList;
import java.util.List;

@With(LoginApiFilter.class)
public class CategoryRestfull extends Controller {

    @Transactional
    public Result remove(String id) {
        Category category = JPA.em().find(Category.class, id);
        if (category == null) {
            return notFound();
        } else {
            JPA.em().remove(category);
            return ok();
        }
    }

    @Transactional
    public Result update(String id) {

        Category category = JPA.em().find(Category.class, id);
        if (category == null) {
            return notFound();
        }

        JsonNode json = request().body().asJson();
        Form<UpdateCategoryRequest> form = Form.form(UpdateCategoryRequest.class).bind(json);

        if (form.hasErrors()) {
            return badRequest(form.errorsAsJson());
        } else {
            UpdateCategoryRequest update = form.get();
            category.nama = update.nama;

            JPA.em().merge(category);

            return ok(Json.toJson(category));
        }
    }

    @Transactional
    public Result list() {
        List<Category> list = JPA.em().createQuery("select a from Category a", Category.class).getResultList();

        return ok(
                Json.toJson(list)
        );
    }

    @Transactional
    public Result create() {
        JsonNode json = request().body().asJson();
        Category category = Json.fromJson(json, Category.class);

        Form<Category> form = Form.form(Category.class).bind(json);
        if (form.hasErrors()) {

            ObjectNode error = Json.newObject();
            error.put("status", "error");
            error.put("messages", form.errorsAsJson());

            return ok(error);

        } else {
            JPA.em().persist(category);
            return ok(
                    Json.toJson(category)
            );
        }
    }


}
