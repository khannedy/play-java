package controllers;

import filters.LoginFilter;
import models.Category;
import play.data.Form;
import play.data.validation.ValidationError;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.*;

import java.util.Arrays;
import java.util.List;

@With(LoginFilter.class)
public class CategoryController extends Controller {

    @Transactional
    public Result remove(String id) {
        Category category = JPA.em().find(Category.class, id);

        JPA.em().remove(category);

        flash("info", "Kategori dengan id " + category.id + " berhasil di hapus");

        return redirect(routes.CategoryController.index());
    }

    public Result newCategory() {
        Form<Category> form = Form.form(Category.class).fill(new Category());
        return ok(views.html.categories.create.render(form));
    }

    @Transactional
    public Result create() {
        Form<Category> form = Form.form(Category.class).bindFromRequest(request().body().asFormUrlEncoded());

        if (form.hasErrors()) {
            // error
            return ok(views.html.categories.create.render(form));
        } else {
            // sukses, save ke database
            Category category = form.get();

            // cek id apakah duplicate
            if (JPA.em().find(Category.class, category.id) != null) {
                // sudah ada kategori dengan id yang di input

                form.errors().put("id", Arrays.asList(new ValidationError("id", "Id sudah terdaftar")));
                return ok(views.html.categories.create.render(form));

            } else {
                // id tidak ada, save data
                JPA.em().persist(category);
                flash("info", "Sukses membuat kategori dengan id " + category.id);
                return redirect(routes.CategoryController.index());
            }
        }
    }

    @Transactional
    public Result edit(String id) {
        Category category = JPA.em().find(Category.class, id);

        Form<Category> form = Form.form(Category.class).fill(category);

        return ok(views.html.categories.update.render(id, form));
    }

    @Transactional
    public Result update(String id) {

        Form<Category> form = Form.form(Category.class).bindFromRequest(request().body().asFormUrlEncoded());

        if (form.hasErrors()) {
            // gagal, display ulang form nya

            return ok(views.html.categories.update.render(id, form));

        } else {
            // save ke database

            Category category = form.get();
            category.id = id;

            JPA.em().merge(category);

            flash("info", "Sukses mengubah kategori dengan id " + category.id);
            return redirect(routes.CategoryController.index());
        }
    }

    @Transactional
    public Result index() {
        List<Category> list = JPA.em().createQuery("select a from Category a", Category.class).getResultList();
        return ok(views.html.categories.index.render(list));
    }

}
