package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import filters.LoginApiFilter;
import models.Category;
import models.CreateProductRequest;
import models.Product;
import models.UpdateCategoryRequest;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.util.List;
import java.util.UUID;

@With(LoginApiFilter.class)
public class ProductRestfull extends Controller {

    @Transactional
    public Result create() {
        JsonNode json = request().body().asJson();
        Form<CreateProductRequest> form = Form.form(CreateProductRequest.class).bind(json);

        if (form.hasErrors()) {

            ObjectNode error = Json.newObject();
            error.put("status", "error");
            error.put("messages", form.errorsAsJson());

            return ok(error);

        } else {
            CreateProductRequest request = form.get();

            Product product = request.toProduct();
            product.id = UUID.randomUUID().toString();
            product.kategori = JPA.em().find(Category.class, request.idKategori);

            JPA.em().persist(product);
            return ok(
                    Json.toJson(product)
            );
        }
    }


}
