package filters;

import controllers.routes;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

/**
 * @author Eko Kurniawan Khannedy
 * @since 24/11/15
 */
public class LoginFilter extends Action.Simple{

    @Override
    public F.Promise<Result> call(Http.Context context) throws Throwable {
        if("ok".equals(context.session().get("login"))){
            // sudah login
            return delegate.call(context);
        }else{
            // belum login
            return F.Promise.pure(
                    redirect(routes.LoginController.login())
            );
        }
    }
}
