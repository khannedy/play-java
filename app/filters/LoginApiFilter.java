package filters;

import controllers.routes;
import models.ApiKey;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

/**
 * @author Eko Kurniawan Khannedy
 * @since 24/11/15
 */
public class LoginApiFilter extends Action.Simple {

    @Transactional
    @Override
    public F.Promise<Result> call(Http.Context context) throws Throwable {

        String id = context.request().getHeader("Api-Key");
        if (id == null) {
            // apikey tidak ada di header
            return F.Promise.pure(unauthorized());
        }

        ApiKey apiKey = JPA.em().find(ApiKey.class, id);
        if (apiKey == null) {
            // apikey tidak ada di database
            return F.Promise.pure(unauthorized());
        }

        // sukses
        return delegate.call(context);
    }
}
